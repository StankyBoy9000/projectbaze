﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace vezbe3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = listBox1.Items[listBox1.SelectedIndex].ToString();
            string result = runQuery(query);
            textBox1.Text = result;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string create_grad = "CREATE TABLE IF NOT EXISTS grad (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "PostanskiBroj nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_pacijent = "CREATE TABLE IF NOT EXISTS pacijent (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Prezime nvarchar(50) not null," +
                "JMBG nvarchar(15) not null," +
                "Telefon nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "Grad_ID bigint not null," +
                "Pol nvarchar(10) not null," +
                "FOREIGN KEY(Grad_ID) REFERENCES grad(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_poslovnaJedinica = "CREATE TABLE IF NOT EXISTS poslovnaJedinica (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Adresa nvarchar(50) not null," +
                "Telefon1 nvarchar(50) not null," +
                "Telefon2 nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_tipOsoblja = "CREATE TABLE IF NOT EXISTS tipOsoblja (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Zarada float null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_osoblje = "CREATE TABLE IF NOT EXISTS osoblje (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Prezime nvarchar(50) not null," +
                "Telefon nvarchar(50) not null," +
                "Email nvarchar(50) not null," +
                "TipOsoblja_ID bigint not null," +
                "FOREIGN KEY(TipOsoblja_ID) REFERENCES tipOsoblja(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_odeljenje = "CREATE TABLE IF NOT EXISTS odeljenje (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "PoslovnaJedinica_ID bigint not null," +
                "FOREIGN KEY(PoslovnaJedinica_ID) REFERENCES poslovnaJedinica(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_izvestaj = "CREATE TABLE IF NOT EXISTS izvestaj (" +
                "ID bigint not null auto_increment," +
                "DatumKreiranja datetime not null," +
                "DatumZatvaranja datetime null," +
                "Anamneza nvarchar(2000) not null," +
                "Pacijent_ID bigint not null," +
                "FOREIGN KEY(Pacijent_ID) REFERENCES pacijent(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_akcija = "CREATE TABLE IF NOT EXISTS akcija (" +
                "ID bigint not null auto_increment," +
                "Naziv nvarchar(50) not null," +
                "Cena float not null," +
                "Trajanje int null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_istorijaIzvestaja = "CREATE TABLE IF NOT EXISTS istorijaIzvestaja (" +
                "ID bigint not null auto_increment," +
                "Datum datetime not null," +
                "Opis nvarchar(2000) not null," +
                "Cena float not null," +
                "Izvestaj_ID bigint not null," +
                "Akcija_ID bigint not null," +
                "Osoblje_ID bigint not null," +
                "Dijagnoza nvarchar(2000) null," +
                "FOREIGN KEY(Izvestaj_ID) REFERENCES izvestaj(ID)," +
                "FOREIGN KEY(Akcija_ID) REFERENCES Akcija(ID)," +
                "FOREIGN KEY(Osoblje_ID) REFERENCES osoblje(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_dobavljac = "CREATE TABLE IF NOT EXISTS dobavljac (" +
                "ID bigint not null auto_increment," +
                "Ime nvarchar(50) not null," +
                "Adresa nvarchar(150) not null," +
                "Email nvarchar(50) null," +
                "Telefon nvarchar(50) not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_opisLeka = "CREATE TABLE IF NOT EXISTS opisLeka (" +
                "ID bigint not null auto_increment," +
                "Tip nvarchar(50) not null," +
                "Naziv nvarchar(50) not null," +
                "Cena float null," +
                "Kolicina bigint not null," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_lek = "CREATE TABLE IF NOT EXISTS lek (" +
                "ID bigint not null auto_increment," +
                "DatumUnosa datetime not null," +
                "NabavnaCena float not null," +
                "Cena float not null," +
                "OpisLeka_ID bigint not null," +
                "Dobavljac_ID bigint not null," +
                "RokTrajanja datetime not null," +
                "FOREIGN KEY(OpisLeka_ID) REFERENCES OpisLeka(ID)," +
                "FOREIGN KEY(Dobavljac_ID) REFERENCES Dobavljac(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_odeljenjeOsoblje = "CREATE TABLE IF NOT EXISTS odeljenjeOsoblje(" +
                "ID bigint not null auto_increment," +
                "Odeljenje_ID bigint not null," +
                "Osoblje_ID bigint not null," +
                "FOREIGN KEY(Odeljenje_ID) REFERENCES Odeljenje(ID)," +
                "FOREIGN KEY(Osoblje_ID) REFERENCES Osoblje(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_sifraDijagnoze = "CREATE TABLE IF NOT EXISTS sifraDijagnoze(" +
                "ID bigint not null auto_increment," +
                "SifraDijagnoze nvarchar(50) not null," +
                "OpisDijagnoze nvarchar(1500) not null," +
                "IstorijaIzvestaja_ID bigint not null," +
                "FOREIGN KEY(IstorijaIzvestaja_ID) REFERENCES IstorijaIzvestaja(ID)," +
                "PRIMARY KEY(ID)" +
                ");";

            string create_lek_istorijaIzvestaja = "CREATE TABLE IF NOT EXISTS lek_istorijaIzvestaja(" +
                "IstorijaIzvestaja_ID bigint not null," +
                "Lek_ID bigint not null," +
                "FOREIGN KEY(IstorijaIzvestaja_ID) REFERENCES IstorijaIzvestaja(ID)," +
                "FOREIGN KEY(Lek_ID) REFERENCES Lek(ID)" +
                ");";

            string resultGrad = runQuery(create_grad);
            string resultPacijent = runQuery(create_pacijent);
            string resultPoslovnaJedinica = runQuery(create_poslovnaJedinica);
            string resultTipOsoblja = runQuery(create_tipOsoblja);
            string resultOsoblje = runQuery(create_osoblje);
            string resultOdeljenje = runQuery(create_odeljenje);
            string resultIzvestaj = runQuery(create_izvestaj);
            string resultAkcija = runQuery(create_akcija);
            string resultIstorijaIzvestaja = runQuery(create_istorijaIzvestaja);
            string resultDobavljac = runQuery(create_dobavljac);
            string resultOpisLeka = runQuery(create_opisLeka);
            string resultLek = runQuery(create_lek);
            string resultOdeljenjeOsoblje = runQuery(create_odeljenjeOsoblje);
            string resultSifraDijagnoze = runQuery(create_sifraDijagnoze);
            string resultLek_istorijaIzvestaja = runQuery(create_lek_istorijaIzvestaja);

            if (!resultGrad.Equals("") && !resultPacijent.Equals("") && !resultPoslovnaJedinica.Equals("")
                && !resultTipOsoblja.Equals("") && !resultOsoblje.Equals("") && !resultOdeljenje.Equals("")
                && !resultIzvestaj.Equals("") && !resultAkcija.Equals("") && !resultIstorijaIzvestaja.Equals("")
                && !resultDobavljac.Equals("") && !resultOpisLeka.Equals("") && !resultLek.Equals("")
                && !resultOdeljenjeOsoblje.Equals("") && !resultSifraDijagnoze.Equals("") && !resultLek_istorijaIzvestaja.Equals(""))
            {
                MessageBox.Show("Tabela je uspesno kreirana!");
            }

        }

        private string runQuery(string text)
        {

            if (text == "")
            {
                MessageBox.Show("There is no query!");
                return "";
            }

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=;database=nova";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            MySqlCommand command = new MySqlCommand(text, databaseConnection);
            command.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                if (text.Split(' ')[0].ToLower().Equals("select"))
                {
                    MySqlDataReader myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {

                        StringBuilder builder = new StringBuilder();
                        while (myReader.Read())
                        {

                            for (int i = 0; i < myReader.FieldCount; i++)
                            {
                                builder.Append(myReader.GetString(i));
                                if (i != myReader.FieldCount - 1)
                                {
                                    builder.Append("-");
                                }
                            }

                            builder.AppendLine();
                        }
                        return builder.ToString();
                    }
                    else
                    {
                        return "Query doesn't have result!";
                    }
                }
                else
                {
                    int numberOfRowsAffected = command.ExecuteNonQuery();
                    return numberOfRowsAffected + "has been affected!";
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return "";
            }
            finally {
                databaseConnection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
                createGradovi();
                createPacijenti();
                createPoslovneJedinice();
                createTipOsoblja();
                createOsoblje();
                createOdeljenje();
                createIzvestaji();
                createAkcija();
                createDobavljaci();
                createOpisLeka();
                createOdeljenjeOsoblje();
                createIstorijaIzvetsaja();
                createLek();
                createLekIstorijaIzvestaja();
                createSifraDijagnoze();
                MessageBox.Show("Uspesno su ispunjene tabele!");
            

            
        }

        private void createSifraDijagnoze()
        {
            Random random = new Random();
            List<String> insertSifreDijagnoze = new List<String>();
            for (int i = 0; i < 900; i++)
            {
                int sifraDijagnoze = random.Next(1, 2000);
                int idIstorijaIzvestaja = random.Next(1, 3000);
                string opis = "Opis";
                insertSifreDijagnoze.Add("insert into sifraDijagnoze values(0,'" + sifraDijagnoze + "','" + opis + "',"+  idIstorijaIzvestaja + ")");
            }
            foreach (string query in insertSifreDijagnoze)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createLekIstorijaIzvestaja()
        {
            Random random = new Random();
            List<String> insertLekoviIzvestaji = new List<String>();
            for (int i = 0; i < 900; i++)
            {
                int idLek = random.Next(1, 2000);
                int idIstorijaIzvestaja = random.Next(1, 3000);
                insertLekoviIzvestaji.Add("insert into lek_istorijaIzvestaja values(" + idIstorijaIzvestaja + "," + idLek+ ")");
            }
            foreach (string query in insertLekoviIzvestaji)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }

        }

        private void createLek()
        {
            Random random = new Random();
            List<String> insertLekovi = new List<String>();
            for (int i = 0; i < 2000; i++) {
                int firstDateYear = random.Next(2005, 2019);
                int firstDateMonth = random.Next(1, 12);
                int firstDateDay = random.Next(1, DateTime.DaysInMonth(firstDateYear, firstDateMonth));


                int randomDay = firstDateDay + random.Next(1, 28);
                int randomMonth = firstDateMonth + random.Next(1, 11);
                int randomYear = firstDateYear + random.Next(1, 3);

                if (randomDay > DateTime.DaysInMonth(firstDateYear, firstDateMonth))
                {
                    randomDay = randomDay - DateTime.DaysInMonth(firstDateYear, firstDateMonth);
                    randomMonth += 1;
                }
                if (randomMonth > 12)
                {
                    randomMonth = randomMonth - 12;
                    randomYear += 1;
                }
                string entryDate = firstDateYear + "-" + firstDateMonth + "-" + firstDateDay;
                string expirationDate = randomYear + "-" + randomMonth + "-" + randomDay;
                int idLeka = random.Next(1, 11);
                int neededPrice = Int32.Parse(runQuery("select opisleka.cena from opisleka where ID =" + idLeka));
                int idDobavljac = random.Next(1, 5);
                double buyingPrice = neededPrice*0.8;
                float finalBuyingPrice = (float)buyingPrice;
                insertLekovi.Add("insert into lek values(0,'" + entryDate + "'," + finalBuyingPrice + "," + neededPrice + "," + idLeka + "," + idDobavljac + ",'" + expirationDate + "')");
            }

            foreach (string query in insertLekovi)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }


        }
        private void createIstorijaIzvetsaja()
        {
            Random random = new Random();
            int idAkcija = 1;
            List<String> insertIzvestaji = new List<String>();
            for (int i = 0; i < 3000; i++)
            {
                int idOsoblja = random.Next(1, 17);
                int idIzvestaj = random.Next(1, 1000);

                if (idOsoblja == 1 || idOsoblja == 10 || idOsoblja == 17)
                {
                    idAkcija = 2;
                } //idAkcija definisana ovde
                else if (idOsoblja >= 2 && idOsoblja <= 4)
                {
                    idAkcija = 1;
                }
                else if (idOsoblja == 5 || idOsoblja == 6)
                {
                    idAkcija = 8;
                }
                else if ((idOsoblja > 6 && idOsoblja < 10) || idOsoblja == 13 || idOsoblja == 14) {
                    idAkcija = 6;
                }
                else if(idOsoblja == 11)
                {
                    idAkcija = 9;
                }
                else if(idOsoblja == 12)
                {
                    idAkcija = 10;
                }
                else if(idOsoblja == 15)
                {
                    idAkcija = 4;
                }
                else if(idOsoblja == 16)
                {
                    idAkcija = 3;
                }

                string neededDate = runQuery("SELECT LEFT(izvestaj.DatumKreiranja , 10) from izvestaj where ID=" + idIzvestaj);
                string price = runQuery("SELECT akcija.Cena from akcija WHERE id=" +idAkcija);
                string desc = "Opis";
                insertIzvestaji.Add("insert into istorijaIzvestaja values(0,'" + neededDate + "','" + desc + "','" + price + "'," + idIzvestaj + "," + idAkcija + "," + idOsoblja +",'Dijagnoza'" + ")");
            }

            foreach (string query in insertIzvestaji)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }


        }

        private void createOdeljenjeOsoblje()
        {
            string[] insertQueries = { "insert into odeljenjeOsoblje values (0,1,1)",
                "insert into odeljenjeOsoblje values (0,1,2)",
                "insert into odeljenjeOsoblje values (0,2,13)",
                "insert into odeljenjeOsoblje values (0,2,3)",
                "insert into odeljenjeOsoblje values (0,2,14)",
                "insert into odeljenjeOsoblje values (0,3,8)",
                "insert into odeljenjeOsoblje values (0,3,4)",
                "insert into odeljenjeOsoblje values (0,4,9)",
                "insert into odeljenjeOsoblje values (0,5,7)",
                "insert into odeljenjeOsoblje values (0,5,6)",
                "insert into odeljenjeOsoblje values (0,6,11)",
                "insert into odeljenjeOsoblje values (0,7,12)",
                "insert into odeljenjeOsoblje values (0,8,10)",
                "insert into odeljenjeOsoblje values (0,9,9)",
                "insert into odeljenjeOsoblje values (0,10,15)",
                "insert into odeljenjeOsoblje values (0,11,17)",
                "insert into odeljenjeOsoblje values (0,11,5)",

            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createOpisLeka()
        {
            string[] insertQueries = { "insert into opisLeka values (0,'Antipiretik','Aspirin',300,400)",
               "insert into opisLeka values (0,'Fizioloski rastvor','NaCl',200,1000)",
               "insert into opisLeka values (0,'Lek za smirenje','Bromazepam',500,500)",
               "insert into opisLeka values (0,'Antiseptik','Rivanol',180,200)",
               "insert into opisLeka values (0,'Antibiotik','Azitromicin',1200,400)",
               "insert into opisLeka values (0,'Butamirat','Sinetus',900,200)",
               "insert into opisLeka values (0,'Antidijabetik','Glucophage',600,100)",
               "insert into opisLeka values (0,'Nepoznat','Eftil',100,400)",
               "insert into opisLeka values (0,'H2-Blokator','Famotidin Alkaloid',300,400)",
               "insert into opisLeka values (0,'Nepoznat','Atacor',300,400)",
               "insert into opisLeka values (0,'Analgetik','Novalgetol',90,900)",
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createDobavljaci()
        {
            string[] insertQueries = { "insert into dobavljac values (0,'Herbalife','Sinise Mihajlovica 4 - Beograd','herbalife@gmail.com','06561123379')",
               "insert into dobavljac values (0,'MedicoSr','Isidore Bjelice 2 - Beograd','medicosr@gmail.com','06321123379')",
               "insert into dobavljac values (0,'Medic','Nikole Tesle 11 - Novi Sad','medic@gmail.com','06111123379')",
               "insert into dobavljac values (0,'Lekovi-Srbije','Branislava Nusica 19 - Nis','lekovisrbije@gmail.com','06561444379')",
               "insert into dobavljac values (0,'naturalherb','Karadjordjeva 11 - Leskovac','naturalherb@gmail.com','06561123322')"
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createAkcija()
        {
            string[] insertQueries = { "insert into akcija values (0,'Vadjenje krvi',300,5)",
                "insert into akcija values (0,'Opsti pregled',2000,20)",
                "insert into akcija values (0,'Internisticki pregled',3000,30)",
                "insert into akcija values (0,'Kardioloski pregled',3000,45)",
                "insert into akcija values (0,'Hirurski pregled',3000,20)",
                "insert into akcija values (0,'Hirurska intervencija',10000,120)",
                "insert into akcija values (0,'Plasticna hirurgija',20000,160)",
                "insert into akcija values (0,'Bris grla',300,2)",
                "insert into akcija values (0,'Ginekoloski pregled',3000,20)",
                "insert into akcija values (0,'Uroloski pregled',3000,20)"
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createIzvestaji()
        {
            Random random = new Random();
            List<String> insertIzvestaji = new List<String>();
            for (int i = 0;i < 1000; i++)
            {
                int firstDateYear = random.Next(2005, 2019);
                int firstDateMonth = random.Next(1, 12);
                int firstDateDay = random.Next(1, DateTime.DaysInMonth(firstDateYear, firstDateMonth));
               

                int randomDay = firstDateDay + random.Next(1, 28);  
                int randomMonth = firstDateMonth + random.Next(1, 11);
                int randomYear = firstDateYear + random.Next(1, 3);

                if (randomDay > DateTime.DaysInMonth(firstDateYear, firstDateMonth))
                {
                    randomDay = randomDay - DateTime.DaysInMonth(firstDateYear, firstDateMonth);
                    randomMonth += 1;
                }
                if(randomMonth > 12)
                {
                    randomMonth = randomMonth - 12;
                    randomYear += 1;
                }
                string firstDate = firstDateYear + "-" + firstDateMonth + "-" + firstDateDay;
                string lastDate = randomYear + "-" + randomMonth + "-" + randomDay;
                string anamneza = "Anamneza";
                int pacijentID = random.Next(1, 1000);
                insertIzvestaji.Add("insert into izvestaj values(0,'"+firstDate+"','"+lastDate+"','"+anamneza+"'," +pacijentID+")");
            }
            foreach (string query in insertIzvestaji)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createOdeljenje()
        {
            string[] insertQueries = { "insert into odeljenje values (0,'Opsta Praksa',1)", //1  2
                "insert into odeljenje values (0,'Hirurgija',3)",//13  3  14
                "insert into odeljenje values (0,'Hitna Sluzba',3)",//8  4
                "insert into odeljenje values (0,'Hitna Sluzba',1)",//9
                "insert into odeljenje values (0,'Prijemna Ambulanta',1)",//7  6
                "insert into odeljenje values (0,'Ginekologija',2)",//11
                "insert into odeljenje values (0,'Urologija',2)",//12
                "insert into odeljenje values (0,'Opsta Praksa',2)",//10
                "insert into odeljenje values (0,'Internisticko Odeljenje',1)",//9
                "insert into odeljenje values (0,'Kardiologija',1)",//15
                "insert into odeljenje values (0,'Medicina Rada',3)"//17  5

            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }
        private void createOsoblje()
        {
            string[] insertQueries = { "insert into osoblje values (0,'Marko','Stanojevic','0645557711','markostanojevic@gmail.com',1)",//1
                "insert into osoblje values (0,'Aleksandra','Gombos','0645337711','aleksandragombos@gmail.com',2)",//2
                "insert into osoblje values (0,'Tina','Stojilkovic','0645557722','tinastojilkovic@gmail.com',2)",//3
                "insert into osoblje values (0,'Valentina','Mitic','0635557711','valentinamitic@gmail.com',2)",//4
                "insert into osoblje values (0,'Natasa','Jotic','06453327711','natasajotic@gmail.com',2)",//5
                "insert into osoblje values (0,'Jelena','Stanisic','0645544411','jelenastanisic@gmail.com',2)",//6
                "insert into osoblje values (0,'Nina','Buha','0621157711','ninabuha@gmail.com',3)",//7
                "insert into osoblje values (0,'Nikolina','Djordjevic','0651557711','nikolinadjordjevic@gmail.com',3)",//8
                "insert into osoblje values (0,'Andjela','Kovacevic','064332111','andjelakovacevic@gmail.com',3)",//9
                "insert into osoblje values (0,'Andrea','Biskupovic','0648897711','andreabiskupovic@gmail.com',1)",//10
                "insert into osoblje values (0,'Nemanja','Spasic','0645557700','nemanjaspasic@gmail.com',4)",//11
                "insert into osoblje values (0,'Marko','Zahorodni','0645332711','markozahorodni@gmail.com',5)",//12
                "insert into osoblje values (0,'Mina','Gusman','0645550001','minagus@gmail.com',6)",//13
                "insert into osoblje values (0,'Bogdan','Teofanovic','0615551711','bogteofanovic@gmail.com',7)",//14
                "insert into osoblje values (0,'Stefan','Komarica','0699957711','stefankom@gmail.com',8)",//15
                "insert into osoblje values (0,'Milos','Komarica','0639002133','miloskomarica@gmail.com',9)",//16
                "insert into osoblje values (0,'Biljana','Dobric','0635254711','biljanadobric@gmail.com',1)",//17
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createTipOsoblja()
        {
            string[] insertQueries = { "insert into tipOsoblja values (0,'Doktor Opste Prakse',90000)",//1
                "insert into tipOsoblja values (0,'Medicinska Sestra',40000)",//2
                "insert into tipOsoblja values (0,'Glavna Medicinska Sestra',50000)",//3
                "insert into tipOsoblja values (0,'Ginekolog',100000)",//4
                "insert into tipOsoblja values (0,'Urolog',100000)",//5
                "insert into tipOsoblja values (0,'Hirurg',120000)",//6
                "insert into tipOsoblja values (0,'Plasticni Hirurg',150000)",//7
                "insert into tipOsoblja values (0,'Kardiolog',110000)",//8
                "insert into tipOsoblja values (0,'Internista',110000)"//9
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createPoslovneJedinice()
        {
            string[] insertQueries = { "insert into poslovnaJedinica values (0,'Med+ Grbavica','Mise Dimitrijevica 3','0653398800','064111222334','medplusgrbavica@gmail.com')",
                "insert into poslovnaJedinica values (0,'Med+ Centar','Zmaj Jovina 33','0653398801','064111222335','medpluscentar@gmail.com')",
                "insert into poslovnaJedinica values (0,'Med+ Podbara','Laze Kostica 43','0653398802','064111222336','medpluspodbara@gmail.com')"
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createGradovi()
        {
            string[] insertQueries = { "insert into grad values (0,'Kovin','26220')",
                "insert into grad values (0,'Novi Sad','21000')",
                "insert into grad values (0,'Beograd','11000')",
                "insert into grad values (0,'Krusevac','37000')",
                "insert into grad values (0,'Novi Pazar','36300')",
                "insert into grad values (0,'Knjazevac','19350')",
                "insert into grad values (0,'Nis','18000')",
                "insert into grad values (0,'Pirot','18300')",
                "insert into grad values (0,'Paracin','35250')",
                "insert into grad values (0,'Leskovac','16000')",
                "insert into grad values (0,'Smederevo','11300')",
                "insert into grad values (0,'Pancevo','26000')",
                "insert into grad values (0,'Valjevo','14000')",
                "insert into grad values (0,'Nova Pazova','22330')",
                "insert into grad values (0,'Stara Pazova','22300')",
                "insert into grad values (0,'Indjija','22320')",
                "insert into grad values (0,'Uzice','31000')",
                "insert into grad values (0,'Kraljevo','36000')",
                "insert into grad values (0,'Subotica','24000')",
                "insert into grad values (0,'Cacak','32000')",
            };

            foreach (string query in insertQueries)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }

        private void createPacijenti()
        {
            string firstNames = @"C:\Users\Marko\Desktop\Baze Podataka\projectbaze\random-name\first-names.txt"; //Ovo se mora promeniti u direktorijum laptopa
            string[] firstNamesList = File.ReadAllLines(firstNames);
            string names = @"C:\Users\Marko\Desktop\Baze Podataka\projectbaze\random-name\names.txt"; //Ovo se mora promeniti u direktorijum laptopa
            string[] namesList = File.ReadAllLines(names);
            //foreach (string name in firstNamesList)

            List<String> insertPacijenti = new List<String>();
            //[ "insert into kupci values (0,'Marko','Markovic','Cacak','1995-2-3',1000)",
            //"insert into kupci values (0,'Ivan','Ivanovic','Beograd','1990-4-4',2500)",
            Random random = new Random();
            for (int i = 0; i < 1000; i++)
            {
                int randomFirstNameNumber = random.Next(4000);
                int randomNameNumber = random.Next(20000);
                
                String ime = firstNamesList[randomFirstNameNumber];
                String prezime = namesList[randomNameNumber];
                String jmbg = random.Next(100000,999999) + "" + random.Next(1000000,9999999);
                String telefon = "06" + random.Next(10000000,99999999);
                String email = ime.ToLower() + prezime.ToLower() + "@gmail.com";
                String pol = "muski";
                if (random.Next(10) % 2 == 0)
                {
                    pol = "zenski";
                }

                insertPacijenti.Add("insert into pacijent values (0,'" + ime + "','" + prezime + "','" + jmbg + "','" + telefon + "','" + email + "'," + random.Next(1,20) + ",'" + pol + "')");
                
            }


            foreach (string query in insertPacijenti)
            {
                string result = runQuery(query);
                if (result.Equals(""))
                {
                    return;
                }
            }
        }
    }
}
